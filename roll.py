# Copyright (c) 2021, Cameron Himes
# All rights reserved.
# See LICENSE for details or go here: https://opensource.org/licenses/BSD-3-Clause

# Imports
import time
import os
from blessed import Terminal

# Settings
FPS = 60
IMAGE_DIR = "./txt/"
CENTER_SCREEN = False
FRAME_COUNTER = False
PROGRESS_COUNTER = False
IMAGE_HEIGHT = 25
IMAGE_WIDTH = 58

# Constants
timeout = 1 / FPS
term = Terminal()


def get_height():
    if term.height == 0:
        return IMAGE_HEIGHT
    else:
        return term.height


def get_width():
    if term.width == 0:
        return IMAGE_WIDTH
    else:
        return term.width


def draw_info(image, number, total):
    print(term.move_up * (FRAME_COUNTER + PROGRESS_COUNTER), end='')
    if PROGRESS_COUNTER:
        print("[Progress: %3.3f%%]" % ((number/total)*100))
    if FRAME_COUNTER:
        print("[Showing frame {0} of {1}]".format(number, total))


def draw_image_centered(image, number, total):
    with open(image, "r") as f:
        buff = [x.removesuffix('\n') for x in f.readlines()]
    print(term.clear + term.home, end='')
    if get_height() > IMAGE_HEIGHT:
        for _ in range((get_height() - IMAGE_HEIGHT) // 2):
            print()
    else:
        for _ in range((IMAGE_HEIGHT - get_height()) // 2):
            buff.pop(0)
        for _ in range((IMAGE_HEIGHT - get_height()) // 2):
            buff.pop()
    for line in buff:
        if get_width() > IMAGE_WIDTH:
            for _ in range((get_width() - IMAGE_WIDTH) // 2):
                print(" ", end="")
            print(line)


def draw_image_normal(image, number, total):
    with open(image, "r") as f:
        buff = [x.removesuffix('\n') for x in f.readlines()]
    print(term.clear + term.home, end='')
    for line in buff:
        print(line)


def draw_frame(image, number, total):
    if CENTER_SCREEN:
        draw_image_centered(image, number, total)
    else:
        draw_image_normal(image, number, total)
    draw_info(image, number, total)


def play_video():
    images = os.listdir(IMAGE_DIR)
    images.sort()
    frame_total = len(images)
    for frame_number, file in enumerate(images):
        draw_frame(IMAGE_DIR + file, frame_number, frame_total)
        time.sleep(timeout)


if __name__ == "__main__":
    try:
        play_video()
    except KeyboardInterrupt:
        exit()
