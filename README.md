# RollSH - an ASCII Rick Roll program

This program plays Rick Astley's Never Gonna Give You Up inside a text-based terminal.

![screenshot](screenshot.png)

# Dependencies

The following are required:

- Python 3.9 or higher
- The `blessed` Python library ([link](https://pypi.org/project/blessed/))

The following are required for building only:

- `youtube-dl`
- `ffmpeg`
- `jp2a`

# Usage

`./rollsh`

Some options can be changed by editing the variables inside `roll.py`.

| Variable         | Type | Description                                      |
| :--------------- | :--- | :----------------------------------------------- |
| FPS              | int  | Number of images to draw per second              |
| IMAGE_DIR        | str  | Directory containing image files                 |
| CENTER_SCREEN    | bool | Centers the image to the termiinal               |
| FRAME_COUNTER    | bool | Shows the current image number                   |
| PROGRESS_COUNTER | bool | Shows the percent of images previously displayed |
| IMAGE_HEIGHT     | int  | Sets the hight of the image files                |
| IMAGE_WIDTH      | int  | Sets the width of the image files                |

# Building

1. Install [dependencies](#dependencies)
2. Clone this repository: `git clone https://gitlab.com/caton101/rollsh.git`
3. Enter program directory: `cd rollsh`
4. Build data files: `make all`

# Installing

1. Follow the [build instructions](#building)
2. Run this command: `make install`
3. Remove the dependencies that are not required after building (optional)

# Uninstalling

1. Run this command: `make uninstall`
2. Remove the dependencies (optional)

